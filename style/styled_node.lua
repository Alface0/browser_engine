local StyledNode = {}

function StyledNode.display(styled_node)
    return styled_node.properties["display"] or "inline"
end

function StyledNode.new(node, properties, children)
    local styled_node = {}
    styled_node.node = node or {}
    styled_node.properties = properties or {}
    styled_node.children = children or {}

    return styled_node
end

return StyledNode