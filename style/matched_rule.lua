local MatchedRule = {}

function MatchedRule.new(rule, a, b, c)
    local matched_rule = {}
    matched_rule.a = a
    matched_rule.b = b
    matched_rule.c = c
    matched_rule.rule = rule

    return matched_rule
end

return MatchedRule