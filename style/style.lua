local SimpleSelector = require "css.structures.simple_selector"

local MatchedRule = require "style.matched_rule"
local StyledNode = require "style.styled_node"

local utils = require "utils"

local Style = {}

function Style.new(dom, rules)
    local style = {}
    style.dom = dom or {}
    style.rules = rules or {}

    function style:get_element_id(elem)
        return elem.attributes["id"] or nil
    end

    function style:get_element_classes(elem)
        local classes = {}
        -- Split by spaces
        for token in string.gmatch(elem.attributes["class"] or "", "[^%s]+") do
            table.insert(classes, token)
        end

        return classes
    end

    function style:matches_simple_selector(elem, selector)
        if selector.tag_name and selector.tag_name ~= elem.tag then
            return false
        end

        if selector.id and selector.id ~= self:get_element_id(elem) then
            return false
        end

        local classes = self:get_element_classes(elem)

        -- Slow match, im sorry
        if #selector.class ~= 0 then
            for _, class in ipairs(selector.class) do
                local matched_class = false
                for _, elem_class in ipairs(classes) do
                    if class == elem_class then
                        matched_class = true
                    end
                end

                -- Element must match all classes of the selector
                if not matched_class then
                    return false
                end
            end
        end

        return true
    end

    function style:matches(element_data, selector)
        return self:matches_simple_selector(element_data, selector)
    end

    function style:match_rule(elem, rule)
        for _, selector in ipairs(rule.selectors) do
            if self:matches(elem, selector) then
                local a, b, c = SimpleSelector.specificity(selector)
                return MatchedRule.new(rule, a, b, c)
            end
        end

        return nil
    end

    function style:matching_rules(elem, rules)
        local matched_rules = {}
        for _, rule in ipairs(rules) do
            local matched_rule = self:match_rule(elem, rule)
            if matched_rule then
                table.insert(matched_rules, matched_rule)
            end
        end
        return matched_rules
    end

    function style:specified_values(elem, rules)
        local values = {}
        local matching_rules = self:matching_rules(elem, rules)

        table.sort(matching_rules, function(a, b)
            return utils.compare_specificity(a, b)
        end)
        
        for _, rule in ipairs(matching_rules) do
            for _, declaration in ipairs(rule.rule.declarations) do
                values[declaration.name] = declaration.value
            end
        end

        return values
    end

    function style:style_tree(node, rules)
        local properties = {}

        local node_type = node.type.text
        if node_type == "elem" then
            properties = self:specified_values(node.type.data, rules)
        elseif node_type == "text" then
        else
            print("Invalid node type found: "..node_type)
        end

        local children = {}
        for _, child in ipairs(node.children) do
            table.insert(children, self:style_tree(child, rules))
        end

        return StyledNode.new(node, properties, children)
    end

    return style
end

return Style