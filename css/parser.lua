local SimpleSelector = require "css.structures.simple_selector"
local Declaration = require "css.structures.declaration"
local Color = require "css.structures.color"
local Rule = require "css.structures.rule"

local utils = require "utils"

local validators = require "validators"

local Parser = require "parser"
local CssParser = {}

function CssParser.new(input)
    local parser = Parser.new(input)

    function parser:parse_identifier()
        return self:consume_while(validators.is_valid_identifier_char)
    end

    function parser:parse_simple_selector()
        local selector = SimpleSelector.new(nil, nil, {})
        while not self:eof() do
            local next_char = self:next_char()
            if next_char == "#" then
                self:consume_char()
                selector.id = self:parse_identifier()
            elseif next_char == "." then
                self:consume_char()
                table.insert(selector.class, self:parse_identifier())
            elseif next_char == "*" then
                self:consume_char()
            elseif validators.is_valid_identifier_char(next_char) then
                selector.tag_name = self:parse_identifier()
            else
                break
            end
        end

        return selector
    end

    function parser:parse_selectors()
        local selectors = {}
        while true do
            table.insert(selectors, self:parse_simple_selector())

            self:consume_whitespace()
            local next_char = self:next_char()
            if next_char == "," then
                self:consume_char()
                self:consume_whitespace()
            elseif next_char == "{" then
                break
            else
                print("Error, invalid character found in selector -> ".. next_char)
                print(inspect(selectors))
                assert(false)
            end
        end
        table.sort(selectors, function(a, b)
            local a_a, a_b, a_c = SimpleSelector.specificity(a)
            local b_a, b_b, b_c = SimpleSelector.specificity(b)
            return utils.compare_specificity(
                {a=a_a, b=a_b, c=a_c},
                {a=b_a, b=b_b, c=b_c}
            )
        end)
        return selectors
    end

    function parser:parse_float()
        return self:consume_while(validators.is_float_char)
    end

    function parser:parse_unit()
        local unit = self:parse_identifier()
        if unit == "px" then
            return unit
        end

        return ""
    end

    function parser:parse_length()
        return {self:parse_float(), self:parse_unit()}
    end

    function parser:parse_hex_pair()
        local hex_pair = string.sub(self.input, self.position, self.position + 1)
        self.position = self.position + 2
        return tonumber(hex_pair, 16)
    end

    function parser:parse_color()
        assert(self:consume_char(), "#")
        return Color.new(
            self:parse_hex_pair(),
            self:parse_hex_pair(),
            self:parse_hex_pair(),
            255
        )
    end

    function parser:parse_value()
        local result
        local next_char = self:next_char()

        if next_char:match("[0-9]") then
            result = self:parse_length()
        elseif next_char == "#" then
            result = self:parse_color()
        else
            result = self:parse_identifier()
        end

        return result
    end

    function parser:parse_declaration()
        local property_name = self:parse_identifier()
        self:consume_whitespace()
        assert(self:consume_char(), ":")
        self:consume_whitespace()
        local value = self:parse_value()
        self:consume_whitespace()
        assert(self:consume_char(), ";")

        return Declaration.new(property_name, value)
    end

    function parser:parse_declarations()
        local declarations = {}
        assert(self:consume_char(), "{")
        while true do
            self:consume_whitespace()
            if self:next_char() == "}" then
                self:consume_char()
                break
            end

            table.insert(declarations, self:parse_declaration())
        end

        return declarations
    end

    function parser:parse_rule()
        return Rule.new(self:parse_selectors(), self:parse_declarations())
    end

    function parser:parse_rules()
        local rules = {}
        while true do
            self:consume_whitespace()
            if self:eof() then
                break
            end
            table.insert(rules, self:parse_rule())
        end

        return rules
    end

    function parser:parse()
        return self:parse_rules()
    end

    return parser
end

return CssParser