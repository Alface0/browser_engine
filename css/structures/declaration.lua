local Declaration = {}

function Declaration.new(name, value)
    local declaration = {}
    declaration.name = name or ""
    declaration.value = value or {}

    return declaration
end

return Declaration