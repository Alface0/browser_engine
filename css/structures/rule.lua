local Rule = {}

function Rule.new(selectors, declarations)
    local rule = {}
    rule.selectors = selectors or {}
    rule.declarations = declarations or {}

    return rule
end

return Rule