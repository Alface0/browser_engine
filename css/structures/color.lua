local Color = {}

function Color.new(r, g, b, a)
    local color = {}
    color.r = r or 0
    color.g = g or 0
    color.b = b or 0
    color.a = a or 1

    return color
end

return Color