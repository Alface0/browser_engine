local Value = {}

function Value.new(keyword, length, color)
    local value = {}
    value.keyword = keyword or ""
    value.length = length or 0
    value.color = color or {}

    return value
end

return Value