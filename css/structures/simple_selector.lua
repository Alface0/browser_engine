local SimpleSelector = {}

function SimpleSelector.specificity(selector)
    local a
    if selector.id then
        a = string.len(selector.id)
    else 
        a = 0
    end

    local b = #selector.class
    
    local c
    if selector.tag_name then
        c = string.len(selector.tag_name)
    else
        c = 0
    end

    return a, b, c
end

function SimpleSelector.new(tag_name, id, class)
    local selector = {}
    selector.tag_name = tag_name or nil
    selector.id = id or nil
    selector.class = class or {}

    return selector
end

return SimpleSelector