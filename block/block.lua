local to_px = require "utils".to_px
local Dimensions = require "box.dimensions"
local inspect = require "inspect"

local Block = {}

function Block.new()
    local block = {}

    function block:calculate_block_width(layout_box, containing_block)
        local properties = layout_box.box_type.node.properties or {}
        
        -- Get lateral sizes
        local width = properties["width"] or "auto"

        local margin_left = properties["margin-left"] or properties["margin"] or {0, "px"}
        local margin_right = properties["margin-right"] or properties["margin"] or {0, "px"}
        
        local border_left = properties["border-left-width"] or properties["border-width"] or {0, "px"}
        local border_right = properties["border-right-width"] or properties["border-width"] or {0, "px"}

        local padding_left = properties["padding-left"] or properties["padding"] or {0, "px"}
        local padding_right = properties["padding-right"] or properties["padding"] or {0, "px"}

        -- Calculate minimum needed box size 
        local total = to_px(width) + to_px(margin_left) + to_px(margin_right) + to_px(border_left) +
            to_px(border_right) + to_px(padding_left) + to_px(padding_right)

        -- If width is not auto and the total is wider than the container, treat auto margins as 0.
        if width ~= "auto" and total > containing_block.content.width then
            if margin_left == "auto" then
                margin_left = {0, "px"}
            end
            if margin_right == "auto" then
                margin_right = {0, "px"}
            end
        end

        -- Check if the content underflows
        local underflow = containing_block.content.width - total

        if width == "auto" then
            if margin_left == "auto" then
                margin_left = {0, "px"}
            end

            if margin_right == "auto" then
                margin_right = {0, "px"}
            end

            if underflow >= 0 then
                -- Expand to fill underflow
                width = {underflow, "px"}
            else
                -- Width cant be negative, adjust the right margin
                width = {0, "px"}
                margin_right = {to_px(margin_right) + underflow, "px"}
            end
        else
            if margin_left == "auto" then
                if margin_right == "auto" then                    
                    margin_left = {underflow / 2, "px"}
                    margin_right = {underflow / 2, "px"}
                else
                    margin_left = {underflow, "px"}
                end
            else
                if margin_right == "auto" then
                    margin_right = {underflow, "px"}
                else
                    margin_right = {to_px(margin_right) + underflow, "px"}
                end
            end
        end

        local dim = layout_box.dimensions
        dim.content.width = to_px(width)

        dim.padding.left = to_px(padding_left)
        dim.padding.right = to_px(padding_right)

        dim.border.left = to_px(border_left)
        dim.border.right = to_px(border_right)

        dim.margin.left = to_px(margin_left)
        dim.margin.right = to_px(margin_right)
    end

    function block:calculate_block_position(layout_box, containing_block)
        local properties = layout_box.box_type.node.properties or {}
        local dim = layout_box.dimensions

        dim.margin.top = to_px(properties["margin-top"] or properties["margin"] or {0, "px"})
        dim.margin.bottom = to_px(properties["margin-bottom"] or properties["margin"] or {0, "px"})
        
        dim.border.top = to_px(properties["border-top-width"] or properties["border-width"] or {0, "px"})
        dim.border.bottom = to_px(properties["border-bottom-width"] or properties["border-width"] or {0, "px"})
        
        dim.padding.top = to_px(properties["padding-top"] or properties["padding"] or {0, "px"})
        dim.padding.bottom = to_px(properties["padding-bottom"] or properties["padding"] or {0, "px"})

        dim.content.x = containing_block.content.x + dim.margin.left + dim.border.left + dim.padding.left

        -- Position box bellow other boxes in the container
        dim.content.y = containing_block.content.y + containing_block.content.height + dim.margin.top + dim.border.top + dim.padding.top
    end

    function block:layout_block_children(layout_box)
        local dim = layout_box.dimensions
        for _, child in ipairs(layout_box.children) do
            self:layout(child, dim)
            dim.content.height = dim.content.height + Dimensions.margin_box(child.dimensions).height
        end
    end

    function block:calculate_block_height(layout_box)
        local properties = layout_box.box_type.node.properties or {}
        if properties["height"] then
            layout_block.dimensions.height = properties["height"]
        end
    end

    function block:layout_block(layout_box, containing_block)
        self:calculate_block_width(layout_box, containing_block)
        self:calculate_block_position(layout_box, containing_block)
        self:layout_block_children(layout_box)
        self:calculate_block_height(layout_box)
    end

    function block:layout(layout_box, containing_block)
        local type = layout_box.box_type.type
        if type == "block" then
            self:layout_block(layout_box, containing_block)
        elseif type == "inline" then
            -- TODO behave as block temporarly
            self:layout_block(layout_box, containing_block)
        elseif type == "anonymous" then
            -- TODO behave as block temporarly
            self:layout_block(layout_box, containing_block)
        else
            assert(false, "Invalid block type")
        end

        return layout_box
    end

    return block
end

return Block