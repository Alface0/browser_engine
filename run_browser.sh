#!/bin/bash

WIDTH=600
HEIGHT=400

if [ $# -lt 2 ]; then
    echo "Usage: run_browser :html_file: :css_file:"
    exit 0
fi

cat $1 | luajit operation/parse_html.lua > dom_tree.json
cat $2 | luajit operation/parse_css.lua > style_rules.json
luajit operation/style_tree.lua dom_tree.json style_rules.json | luajit operation/layout_tree.lua | luajit operation/block_layout.lua $WIDTH | luajit operation/image_render.lua $WIDTH $HEIGHT

rm dom_tree.json
rm style_rules.json

exit 0
