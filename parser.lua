-- Parse
local validators = require "validators"

local Parser = {}

function Parser.new(input)
    local parser = {}
    parser.position = 1
    parser.input = input or ""

    function parser:next_char()
        return string.sub(self.input, self.position, self.position)
    end

    function parser:starts_with(text)
        local next_text = string.sub(self.input, self.position, self.position + #text - 1)
        return text == next_text
    end

    function parser:eof()
        return self.position >= #self.input
    end

    function parser:consume_char()
        local char = string.sub(self.input, self.position, self.position)
        self.position = self.position + 1
        return char
    end

    function parser:consume_while(validator)
        local text = ""
        while (not self:eof()) and validator(self:next_char()) do
            text = text .. self:consume_char()
        end

        return text
    end

    function parser:consume_whitespace()
        self:consume_while(validators.is_whitespace)
    end

    function parser:parse()end

    return parser
end

return Parser