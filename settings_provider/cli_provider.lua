local SettingsProvider = require "settings_provider.settings_provider"
local CliProvider = {}

function CliProvider.new(conf)
    local provider = SettingsProvider.new(conf)

    function provider:print_usage()
        local usage = "Usage: program"
        for _, _arg in ipairs(self.conf.args) do
            usage = usage.." <".._arg..">"
        end

        print(usage)
    end

    function provider:get_settings()
        local settings = {}
        for i, _arg in pairs(self.conf.args) do
            local value = arg[i]
            if not value and self.conf.args_mandatory then
                self:print_usage()
                os.exit(-1)
            end

            settings[_arg] = value
        end

        return settings
    end
    
    return provider
end

return CliProvider