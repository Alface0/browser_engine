local SettingsProvider = {}

function SettingsProvider.new(conf)
    local provider = {}
    provider.conf = conf or {}

    function provider:get_settings()end
    
    return provider
end

return SettingsProvider