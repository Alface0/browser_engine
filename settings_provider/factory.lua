local CliProvider = require "settings_provider.cli_provider"

local PROVIDER = {
    cli = CliProvider
}

return function(type, conf)
    local type = type or "cli"
    local conf = conf or {}
    return PROVIDER[type].new(conf)
end