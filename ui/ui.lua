local Dimensions = require "box.dimensions"
local Rect = require "box.rect"
local inspect = require "inspect"
local UI = {}

function UI.new()
    local ui = {}
    ui.display_list = {}

    function ui:get_color(block_layout, name)
        local type = block_layout.box_type.type
        local properties = block_layout.box_type.node.properties
        if type == "inline" or type == "block" then
            if properties[name] then
                return properties[name]
            end
        end

        return nil
    end

    function ui:render_borders(list, block_layout)
        local color = self:get_color(block_layout, "border-color")
        if not color then
            return nil
        end

        local dim = block_layout.dimensions
        local border_box = Dimensions.border_box(dim)

        -- Left Border
        table.insert(list, { 
            "solid_color", {color, Rect.new(
                border_box.x,
                border_box.y,
                dim.border.left,
                border_box.height
            )}
        })

        -- Right Border
        table.insert(list, { 
            "solid_color", {color, Rect.new(
                border_box.x + border_box.width - dim.border.right,
                border_box.y,
                dim.border.right,
                border_box.height
            )}
        })

        -- Top Border
        table.insert(list, { 
            "solid_color", {color, Rect.new(
                border_box.x,
                border_box.y,
                border_box.width,
                dim.border.top
            )}
        })

        -- Bottom Border
        table.insert(list, { 
            "solid_color", {color, Rect.new(
                border_box.x,
                border_box.y + border_box.width - dim.border.bottom,
                border_box.width,
                dim.border.bottom
            )}
        })
    end

    function ui:render_background(list, block_layout)
        local color = self:get_color(block_layout, "background")
        if color then
            local inspect = require "inspect"
            table.insert(list, {"solid_color", {color, Dimensions.border_box(block_layout.dimensions)}})
        end
    end

    function ui:render_layout_box(list, block_layout)
       self:render_background(list, block_layout)
       self:render_borders(list, block_layout)

       for _, child in ipairs(block_layout.children) do
           self:render_layout_box(list, child)
       end
    end

    function ui:render_text(list, block_layout)
        if block_layout.box_type.node.node then
            local html_type = block_layout.box_type.node.node.type
            
            if html_type and html_type.text == "text" then
                local color = self:get_color(block_layout, "color") or {r=0, g=0, b=0, a=255}
                table.insert(list, {"text", {html_type.data, color, block_layout.dimensions.content}})
            end
        end
            
        for _, child in ipairs(block_layout.children) do
            self:render_text(list, child)
        end
    end

    function ui:build_display_list(block_layout)
        local list = {}
        self:render_layout_box(list, block_layout)
        self:render_text(list, block_layout)
        return list
    end

    return ui
end

return UI