local inspect = require "inspect"
local CssParser = require "css.parser"
local HtmlParser = require "html.parser"
local Style = require "style.style"

local html_file = io.open("tests/example.html", "r")
local html_text = html_file:read("*all")

local css_file = io.open("tests/example.css", "r")
local css_text = css_file:read("*all")

local html_parser = HtmlParser.new(html_text)
local root_node = html_parser:parse()

local css_parser = CssParser.new(css_text)
local rules = css_parser:parse()

local style = Style.new(root_node, rules)
local style_tree = style:style_tree(style.dom, style.rules)
print(inspect(style_tree))

html_file:close()
css_file:close()