local inspect = require "inspect"
local Parser = require "html.parser"
local html_file = io.open("tests/example.html", "r")

local text = html_file:read("*all")

local parser = Parser.new(text)
local root_node = parser:parse()

print(inspect(root_node))

html_file:close()