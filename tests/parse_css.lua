local inspect = require "inspect"
local Parser = require "css.parser"
local css_file = io.open("tests/example.css", "r")

local text = css_file:read("*all")

local parser = Parser.new(text)
local rules = parser:parse()

print(inspect(rules))

css_file:close()