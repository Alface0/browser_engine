local formatter = require "format.factory"

table_data = {
		name="Daniel",
		age=25,
		country="Portugal",
		tags={1, 2, 3}
}

local json_formatter = formatter("json")
print(json_formatter:encode(table_data))