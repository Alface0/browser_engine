return {
    is_whitespace = function(char)
        if char:match("[ \t\n]") then
            return true
        else
            return false
        end
    end,
    is_alphanumeric = function(char)
        if char:match("[A-Za-z0-9]") then
            return true
        else
            return false
        end
    end,
    not_opening_tag = function(char)
        return char ~= "<"
    end,
    is_valid_identifier_char = function(char)
        if char:match("[a-zA-Z0-9_-]") then
            return true
        else
            return false
        end
    end,
    is_float_char = function(char)
        if char:match("[0-9.]") then
            return true
        else
            return false
        end
    end
}