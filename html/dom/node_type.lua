local NodeType = {}

function NodeType:new(text, element_data)
    local node_type = {}
    node_type.text = text or "undefined"
    node_type.data = element_data or {}
    return node_type
end

return NodeType
