local Node = {}

function Node:new(node_type, children)
	local node = {}
	node.type = node_type
	node.children = children or {}
	return node
end

return Node