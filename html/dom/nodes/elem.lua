local node = require "html.dom.node"
local node_type = require "html.dom.node_type"
local element_data = require "html.dom.element_data"

local Elem = {}

function Elem:new(name, attr, children)
    local data = element_data:new(name, attr)
    local type = node_type:new("elem", data)

    local elem = node:new(type, children)
    return elem
end

return Elem