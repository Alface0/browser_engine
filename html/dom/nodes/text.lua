local node = require "html.dom.node"
local node_type = require "html.dom.node_type"

local Text = {}

function Text:new(text_data)
    local type = node_type:new("text", text_data)

    local text = node:new(type, {})
    return text
end

return Text