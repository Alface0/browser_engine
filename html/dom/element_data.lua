local ElementData = {}

function ElementData:new(tag, attributes)
    local element = {}

    element.tag = tag or ""
    element.attributes = attributes or {}

    return element
end

return ElementData