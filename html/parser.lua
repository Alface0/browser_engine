-- Nodes
local elem_node = require "html.dom.nodes.elem"
local text_node = require "html.dom.nodes.text"

-- Parse
local validators = require "validators"

local Parser = require "parser"
local HtmlParser = {}

function HtmlParser.new(input)
    local parser = Parser.new(input)

    function parser:parse_tag_name()
        return self:consume_while(validators.is_alphanumeric)
    end

    function parser:parse_attr_val()
        local open_quote = self:consume_char()
        assert(open_quote == '"' or open_quote == "'")
        local value = self:consume_while(function(char) return char ~= open_quote end)
        assert(self:consume_char() == open_quote)
        return value
    end

    function parser:parse_attr()
        local tag_name = self:parse_tag_name()
        assert(self:consume_char() == "=")
        local value = self:parse_attr_val()
        return tag_name, value
    end

    function parser:parse_attributes()
        local attributes = {}
        while true do
            self:consume_whitespace()
            if self:next_char() == ">" then
                break
            end

            local attr, value = self:parse_attr()
            attributes[attr] = value
        end

        return attributes
    end

    function parser:parse_text()
        local text = self:consume_while(validators.not_opening_tag)
        return text_node:new(text)
    end

    function parser:parse_element()
        -- Consume opening tag
        assert(self:consume_char() == "<")
        local tag_name = self:parse_tag_name()
        local attr = self:parse_attributes()
        assert(self:consume_char() == ">")

        -- Consume contents
        local children = self:parse_nodes()

        -- Consume closing tag
        assert(self:consume_char() == "<")
        assert(self:consume_char() == "/")
        assert(self:parse_tag_name() == tag_name)
        assert(self:consume_char() == ">")

        return elem_node:new(tag_name, attr, children)
    end

    function parser:parse_node()
        local next_char = self:next_char()
        local node

        if next_char == "<" then
            node = self:parse_element()
        else
            node = self:parse_text()
        end

        return node
    end

    function parser:parse_nodes()
        local nodes = {}
        while true do
            self:consume_whitespace()
            if self:eof() or self:starts_with("</") then
                break
            end

            table.insert(nodes, self:parse_node())
        end

        return nodes
    end

    function parser:parse()
        local nodes = self:parse_nodes()

        if #nodes == 1 then
            return nodes[1]
        else
            return elem_node:new("html", {}, nodes)
        end
    end

    return parser
end

return HtmlParser