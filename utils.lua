local function compare_specificity(a, b)
    if a.a > b.a then
        return true
    end

    if a.b > b.b then
        return true
    end

    if a.c > b.c then
        return true
    end

    return false
end

local function to_px(length)
    if length[2] and length[2] == "px" then
        return length[1]
    else
        return 0.0
    end
end

local function clamp(number, min, max)
    if number < min then
        return min
    elseif number > max then
        return max
    else
        return number
    end
end

return  {
    compare_specificity = compare_specificity,
    to_px = to_px,
    clap = clamp
}