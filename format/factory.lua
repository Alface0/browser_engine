local JsonFormatter = require "format.json_formatter"

local FORMATTERS = {
    json = JsonFormatter
}

return function(formatter_type)
    return FORMATTERS[formatter_type].new()
end