local Formatter = require "format.formatter"
local dkjson = require "dkjson"

local JsonFormatter = {}

function JsonFormatter.new()
    local formatter = Formatter.new()
    
    function formatter:encode(data)
        return dkjson.encode(data, {indent = true})
    end

    function formatter:decode(text)
        local obj, pos, err = dkjson.decode(text, 1, nil)
        if err then
            print("Failed to decode the received text: "..text)
        end

        return obj
    end

    return formatter
end

return JsonFormatter