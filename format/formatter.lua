local Formatter = {}

function Formatter.new()
    local formatter = {}
    
    function formatter:encode()end
    function formatter:decode()end

    return formatter
end

return Formatter