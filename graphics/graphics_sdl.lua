local Graphics = require "graphics.graphics"
local GraphicsSDL = {}

local ffi = require "ffi"
local SDL = ffi.load("libSDL2.so")

-- SDL Imports
ffi.cdef[[
typedef struct SDL_Window SDL_Window;
typedef struct SDL_Renderer SDL_Renderer;
typedef struct SDL_Texture SDL_Texture;
typedef struct SDL_Surface SDL_Surface;

typedef struct SDL_Color { uint8_t r, g, b, a;} SDL_Color;
typedef struct SDL_Rect {int x, y, w, h;} SDL_Rect;

SDL_Window * SDL_CreateWindow(const char *title, int x, int y, int w, int h, uint32_t flags);
int SDL_Init(uint32_t flags);
void SDL_DestroyWindow(SDL_Window * window);
void SDL_Delay(uint32_t ms);
void SDL_Quit(void);
void SDL_FreeSurface(SDL_Surface* surface);

SDL_Renderer* SDL_CreateRenderer(SDL_Window* window, int index, uint32_t flags);
int SDL_RenderDrawRect(SDL_Renderer* renderer, const SDL_Rect * rect);
void SDL_DestroyRenderer(SDL_Renderer* renderer);
void SDL_RenderPresent(SDL_Renderer* renderer);
int SDL_RenderClear(SDL_Renderer* renderer);

SDL_Texture* SDL_CreateTextureFromSurface(SDL_Renderer* renderer, SDL_Surface*  surface);
int SDL_RenderCopy(SDL_Renderer* renderer, SDL_Texture * texture, const SDL_Rect * srcrect, const SDL_Rect * dstrect);

int SDL_RenderDrawRect(SDL_Renderer * renderer, const SDL_Rect* rect);
int SDL_RenderFillRect(SDL_Renderer* renderer, const SDL_Rect* rect);
int SDL_SetRenderDrawColor(SDL_Renderer* renderer, uint8_t r, uint8_t g, uint8_t b, uint8_t a);
]]


local TTF = ffi.load("libSDL2_ttf.so")

-- SDL TTF Imports
ffi.cdef[[
typedef struct _TTF_Font TTF_Font;

int TTF_Init();
void TTF_Quit();
TTF_Font * TTF_OpenFont(const char *file, int ptsize);
SDL_Surface * TTF_RenderText_Solid(TTF_Font *font, const char *text, SDL_Color fg);
]]


function GraphicsSDL.new(width, height)
    local graphics_sdl = Graphics.new()
    function graphics_sdl:init()
        SDL.SDL_Init(32)
        TTF.TTF_Init()

        self.window = SDL.SDL_CreateWindow("Hello World!", 0, 0, width, height, 2)
        self.renderer = SDL.SDL_CreateRenderer(self.window, -1, 0)
        self.font = TTF.TTF_OpenFont("Helvetica-Normal.ttf", 12)

        SDL.SDL_RenderClear(self.renderer)
        SDL.SDL_SetRenderDrawColor(self.renderer, 255, 255, 255, 255)

        local rect = ffi.new("SDL_Rect", {0, 0, width, height})
        SDL.SDL_RenderFillRect(self.renderer, rect)
    end

    function graphics_sdl:solid_color(color, rect)
        local rect = ffi.new("SDL_Rect", {rect.x, rect.y, rect.width, rect.height})
        SDL.SDL_SetRenderDrawColor(self.renderer, color.r, color.g, color.b, color.a)
        SDL.SDL_RenderFillRect(self.renderer, rect)
        SDL.SDL_RenderPresent(renderer)
    end

    function graphics_sdl:text(text, color, rect)
        local text_rect = ffi.new("SDL_Rect", {rect.x, rect.y, #text * 8, 14})
        local text_color = ffi.new("SDL_Color", {color.r, color.g, color.b, color.a})

        self.surface = TTF.TTF_RenderText_Solid(self.font, text, text_color)
        local message = SDL.SDL_CreateTextureFromSurface(self.renderer, self.surface)
        SDL.SDL_RenderCopy(self.renderer, message, nil, text_rect)
        SDL.SDL_RenderPresent(self.renderer)
    end

    function graphics_sdl:release()
        SDL.SDL_DestroyWindow(self.window)
        SDL.SDL_DestroyRenderer(self.renderer)

        if self.surface then
            SDL.SDL_FreeSurface(self.surface)
        end

        TTF.TTF_Quit()
        SDL.SDL_Quit()
    end

    return graphics_sdl
end

return GraphicsSDL
