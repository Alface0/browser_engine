local Graphics = require "graphics.graphics"
local GLove = {}

function GLove.new()
    local g_love = Graphics.new()
    function g_love:solid_color(color, rect)
        love.graphics.setColor(color.r / 255, color.g / 255, color.b / 255, color.a / 255)
        love.graphics.rectangle("fill", rect.x, rect.y, rect.width, rect.height)
    end

    function g_love:text(text, color, rect)
        love.graphics.setColor(color.r / 255, color.g / 255, color.b / 255, color.a / 255)
        love.graphics.print(text, rect.x, rect.y)
    end

    return g_love
end

return GLove