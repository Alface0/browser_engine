local Pixelbuffer = require "graphics.pixelbuffer"
local Love = require "graphics.g_love"
local sdl = require "graphics.graphics_sdl"

local GRAPHICS = {
    pixelbuffer = Pixelbuffer,
    love = Love,
    sdl = sdl
}

return function(type, args)
    return GRAPHICS[type].new(unpack(args or {}))
end
