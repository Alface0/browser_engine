local Graphics = {}

function Graphics.new()
    local graphics = {}
    function graphics:init() end
    function graphics:release() end
    function graphics:solid_color(color, rect)end
    function graphics:text(text, color, rect)end
    function graphics:run_command_list(command_list)
        self:init()
        for _, command in ipairs(command_list) do
            local command_name = command[1]
            local command_arguments = command[2] or {}
            self[command_name](self, unpack(command_arguments))
        end
    end

    return graphics
end

return Graphics
