local Graphics = require "graphics.graphics"
local Color = require "css.structures.color"
local clamp = require "utils".clamp

local Pixelbuffer = {}

function Pixelbuffer.new(width, height)
    local pixelbuffer = Graphics.new()
    pixelbuffer.pixels = {}
    pixelbuffer.width = width or 400
    pixelbuffer.height = height or 300

    for i=1, width * height do
        pixelbuffer.pixels[i] = Color.new(255, 255, 255, 255)
    end

    function pixelbuffer:solid_color(color, rect)
        local x0 = clamp(rect.x, 0, self.width)
        local y0 = clamp(rect.y, 0, self.height)
        local x1 = clamp(rect.x + rect.width, 0, self.width)
        local y1 = clamp(rect.y + rect.height, 0, self.height)

        for y=y0, y1 do
            for x=x0, x1 do
                self.pixels[y*self.width + x] = color
            end
        end
    end

    return pixelbuffer
end

return Pixelbuffer