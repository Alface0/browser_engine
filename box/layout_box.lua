local BoxType = require "box.box_type"
local Dimensions = require "box.dimensions"

local LayoutBox = {}

function LayoutBox.get_inline_container(layout_box)
    local type = layout_box.box_type.type
    if type == "inline" or type == "anonymous" then
        return layout_box
    elseif type == "block" then
        local total_child = #layout_box.children
        if not(total_child > 0 and layout_box.children[total_child].box_type.type == "anonymous") then
            table.insert(layout_box.children, LayoutBox.new(BoxType.new("anonymous")))
        end

        return layout_box.children[#layout_box.children]
    else
        assert(false, "Invalid layout_box type - " .. type)
    end
end

function LayoutBox.new(box_type)
    local layout_box = {}

    layout_box.dimensions = Dimensions.new()
    layout_box.box_type = box_type or {}
    layout_box.children = {}

    return layout_box
end

return LayoutBox