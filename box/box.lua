local LayoutBox = require "box.layout_box"
local BoxType = require "box.box_type"

local StyledNode = require "style.styled_node"

local Box = {}

function Box.new()
    local box = {}

    function box:build_layout_tree(styled_node)
        local node_display = StyledNode.display(styled_node)
        local root = LayoutBox.new(BoxType.new(node_display, styled_node))

        for _, child in ipairs(styled_node.children) do
            local display = StyledNode.display(child)
            if display == "block" then
                table.insert(root.children, self:build_layout_tree(child))
            elseif display == "inline" then
                local container = LayoutBox.get_inline_container(root)
                local children = container.children
                local node_layout_tree = self:build_layout_tree(child)
                table.insert(children, node_layout_tree)
            else            
                -- Skip blocks with none
            end
        end

        return root
    end

    return box
end

return Box