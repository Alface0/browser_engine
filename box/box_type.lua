local BoxType = {}

function BoxType.new(type, node)
    local box_type = {}

    box_type.type = type or "anonymous"
    box_type.node = node or {}

    return box_type
end

return BoxType