local EdgeSizes = {}

function EdgeSizes.new()
    local edge_sizes = {}

    edge_sizes.left = 0
    edge_sizes.right = 0
    edge_sizes.top = 0
    edge_sizes.bottom = 0

    return edge_sizes
end

return EdgeSizes