local Rect = require "box.rect"
local EdgeSizes = require "box.edge_sizes"

local Dimensions = {}

function Dimensions.new(content)
    local dimensions = {}
    dimensions.content = content or Rect.new()

    dimensions.padding = EdgeSizes.new()
    dimensions.border = EdgeSizes.new()
    dimensions.margin = EdgeSizes.new()
    return dimensions
end

function Dimensions.padding_box(dimensions)
    return Rect.expanded_by(dimensions.content, dimensions.padding)
end

function Dimensions.border_box(dimensions)
    return Rect.expanded_by(Dimensions.padding_box(dimensions), dimensions.border)
end

function Dimensions.margin_box(dimensions)
    return Rect.expanded_by(Dimensions.border_box(dimensions), dimensions.margin)
end

return Dimensions
