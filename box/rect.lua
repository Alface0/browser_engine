local Rect = {}

function Rect.new(x, y, width, height)
    local rect = {}

    rect.x = x or 0
    rect.y = y or 0
    rect.width = width or 0
    rect.height = height or 0

    return rect
end

function Rect.expanded_by(rect, edge_sizes)
    return Rect.new(
        rect.x - edge_sizes.left,
        rect.y - edge_sizes.top,
        rect.width + edge_sizes.left + edge_sizes.right,
        rect.height + edge_sizes.top + edge_sizes.bottom
    )
end

return Rect