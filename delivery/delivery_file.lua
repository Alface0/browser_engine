local DeliveryFile = {}

function DeliveryFile.new(conf)
    local delivery = {}
    delivery.filename = conf.filename
    delivery.file = nil

    function delivery:init()
        self.file = io.open(self.filename, "rw")
    end
    
    function delivery:read()
        return self.file:read("*a")
    end
    
    function delivery:write(data)
        return self.file:write(data)
    end
    
    function delivery:release()
        io.close(self.file)
    end

    return delivery
end

return DeliveryFile