local DeliveryStdIO = require "delivery.delivery_stdio"
local DeliveryFile = require "delivery.delivery_file"

local DELIVERY = {
    std = DeliveryStdIO,
    file = DeliveryFile
}

return function(type, conf)
    local type = type or "std"
    local conf = conf or nil
    return DELIVERY[type].new(conf)
end