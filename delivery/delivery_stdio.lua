local Delivery = require "delivery.delivery"

local DeliveryStdIO = {}

function DeliveryStdIO.new(conf)
    local delivery_stdio = Delivery.new()
    
    function delivery_stdio:read()
        return io.stdin:read("*a")
    end

    function delivery_stdio:write(data)
        return io.stdout:write(data)
    end

    return delivery_stdio
end

return DeliveryStdIO