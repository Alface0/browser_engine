local Delivery = {}

function Delivery.new(conf)
    local delivery = {}

    function delivery:init()end
    function delivery:read()end
    function delivery:write(data)end
    function delivery:release()end

    return delivery
end

return Delivery