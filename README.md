# Browser Engine

This is a simple implementation of a browser rendering engine in lua.
Each step can be run in isolation, the main steps of the browser are:

* HTML Parse
* CSS Parse
* Style Tree Generation
* Layout Tree Generation
* Box Layout Generation
* Drawing Command List Creation

## Example

Here we have the result of parsing a css and html file and rendering them with the love2d.

![Love2d example](demo/love2d_example.png)

## Running

Right now only the love2D version is working, make sure you have love2d(version 11.2) installed and then run:
```sh
love operation/love_render/.
```

The sdl version uses the FFI and requires you to have sdl2 and sdl2_ttf in your system. It still has some problem in rendering but if you want to try it run:
```sh
sh run_browser.sh test.html test.css
```

There are the isolated operations in the operations folder. If you want to try some of them isolated to inspect the intermediate trees and data you can do something like:
```sh
cat test.html | luajit operation/parse_html.lua
```
