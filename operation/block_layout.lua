#!/usr/bin/lua5.1
local Operation = require "operation.operation"
local Block = require "block.block"
local SettingsProviderFactory = require "settings_provider.factory"
local Dimensions = require "box.dimensions"
local Rect = require "box.rect"

local settings_provider = SettingsProviderFactory("cli", {
    args = {"width", "block_layout"}
})

local settings = settings_provider:get_settings()

-- Open file if argument received, or read from stdin if nothing was received
local input_config
if settings["block_layout"] then
    input_config = {
        {type = "file", format = "json", conf = {filename = settings["block_layout"]}}
    }
else
    input_config = {
        {type = "std", format = "json"}
    }
end

local output_config = {
    {type = "std", format = "json"}
}

local block_layout = function(self, input_data)
    local layout_tree = input_data[1]

    local block = Block.new()
    return block:layout(layout_tree, Dimensions.new(Rect.new(
        0,
        0,
        tonumber(settings.width) or 600,
        0
    )))
end

local operation = Operation.new(input_config, output_config, block_layout)
operation:run()
return 0
