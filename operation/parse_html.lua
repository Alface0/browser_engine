#!/usr/bin/lua5.1
local Operation = require "operation.operation"
local Parser = require "html.parser"

local SettingsProviderFactory = require "settings_provider.factory"
local settings_provider = SettingsProviderFactory("cli", {
    args = {"html_file"}
})

local settings = settings_provider:get_settings()

-- Open file if argument received, or read from stdin if nothing was received
local input_config
if settings["html_file"] then
    input_config = {
        {type = "file", conf = {filename = settings["html_file"]}}
    }
else
    input_config = {
        {type = "std"}
    }
end

local output_config = {
    {type = "std", format = "json"}
}

local parse_html = function(self, input_data)
    local stdin_data = input_data[1]
    local parser = Parser.new(stdin_data)
    return parser:parse()
end

local operation = Operation.new(input_config, output_config, parse_html)
operation:run()
return 0
