#!/usr/bin/lua5.1
local Operation = require "operation.operation"
local GraphicsFactory = require "graphics.graphics_factory"

local HtmlParser = require "html.parser"
local CssParser = require "css.parser"
local Style = require "style.style"
local Box = require "box.box"
local Block = require "block.block"
local Dimensions = require "box.dimensions"
local Rect = require "box.rect"
local UI = require "ui.ui"

local inspect = require "inspect"

local input_config = {
    {type = "file", conf = {filename="operation/love_render/test.html"}},
    {type = "file", conf = {filename="operation/love_render/test.css"}}
}

local graphics = GraphicsFactory("love", {WIDTH, HEIGHT})
local display_list

local image_render = function(self, input_data)
    -- Html
    local html_parser = HtmlParser.new(input_data[1])
    local html_tree = html_parser:parse()

    -- CSS
    local css_parser = CssParser.new(input_data[2])
    local css_rules = css_parser:parse()

    -- Style
    local style = Style.new(html_tree, css_rules)
    local style_tree = style:style_tree(style.dom, style.rules)

    -- Box
    local box = Box.new()
    local box_tree = box:build_layout_tree(style_tree)

    -- Block
    local block = Block.new()
    local block_layout = block:layout(box_tree, Dimensions.new(Rect.new(0, 0, WIDTH, 0)))
    --print(inspect(block_layout))

    -- UI
    local ui = UI.new()
    display_list = ui:build_display_list(block_layout)
    return {}
end

function love.load()
    local operation = Operation.new(input_config, {}, image_render)
    operation:run()
end
function love.draw()
    love.graphics.setColor(1, 1, 1, 1)
    love.graphics.rectangle("fill", 0, 0, WIDTH, HEIGHT)
    
    graphics:run_command_list(display_list)
end

return 0
