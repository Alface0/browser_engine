#!/usr/bin/lua5.1
local Operation = require "operation.operation"
local Style = require "style.style"
local SettingsProviderFactory = require "settings_provider.factory"

local settings_provider = SettingsProviderFactory("cli", {
    args = {"dom_file", "rules_file"},
    args_mandatory = true
})

local settings = settings_provider:get_settings()

local input_config = {
    {type = "file", format = "json", conf = {filename = settings["dom_file"]}},
    {type = "file", format = "json", conf = {filename = settings["rules_file"]}}
}

local output_config = {
    {type = "std", format = "json"}
}

local style_tree = function(self, input_data)
    local dom_tree = input_data[1]
    local css_rules = input_data[2]
    
    local style = Style.new(dom_tree, css_rules)
    return style:style_tree(style.dom, style.rules)
end

local operation = Operation.new(input_config, output_config, style_tree)
operation:run()
return 0
