#!/usr/bin/lua5.1
local Operation = require "operation.operation"
local SettingsProviderFactory = require "settings_provider.factory"
local GraphicsFactory = require "graphics.graphics_factory"
local UI = require "ui.ui"

local settings_provider = SettingsProviderFactory("cli", {
    args = {"width", "height", "block_layout"}
})

local settings = settings_provider:get_settings()

-- Open file if argument received, or read from stdin if nothing was received
local input_config
if settings["block_layout"] then
    input_config = {
        {type = "file", format = "json", conf = {filename = settings["block_layout"]}}
    }
else
    input_config = {
        {type = "std", format = "json"}
    }
end

local output_config = {}

local graphics = GraphicsFactory("sdl", {
    tonumber(settings.width) or 600,
    tonumber(settings.height) or 400
})

local image_render = function(self, input_data)
    local block_layout = input_data[1]
    local ui = UI.new()
    local display_list = ui:build_display_list(block_layout)

    graphics:run_command_list(display_list)
    os.execute("sleep 5")
    return graphics.pixels
end

local operation = Operation.new(input_config, output_config, image_render)
operation:run()
return 0
