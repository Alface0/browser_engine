#!/usr/bin/lua5.1
local Operation = require "operation.operation"
local Box = require "box.box"
local SettingsProviderFactory = require "settings_provider.factory"

local settings_provider = SettingsProviderFactory("cli", {
    args = {"style_tree"}
})

local settings = settings_provider:get_settings()

-- Open file if argument received, or read from stdin if nothing was received
local input_config
if settings["style_tree"] then
    input_config = {
        {type = "file", format = "json", conf = {filename = settings["style_tree"]}}
    }
else
    input_config = {
        {type = "std", format = "json"}
    }
end

local output_config = {
    {type = "std", format = "json"}
}

local layout_tree = function(self, input_data)
    local style_tree = input_data[1]
    
    local box = Box.new()
    return box:build_layout_tree(style_tree)
end

local operation = Operation.new(input_config, output_config, layout_tree)
operation:run()
return 0
