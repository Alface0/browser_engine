local DeliveryFactory = require "delivery.factory"
local FormatterFactory = require "format.factory"

local Operation = {}

function Operation.new(inputs, outputs, func)
    local operation = {}
    operation.inputs = inputs or nil
    operation.outputs = outputs or nil
    operation.func = func or function(self, input_data)end

    function operation:gather_inputs()
        local input_data = {}
        for i, input in ipairs(self.inputs) do
            local type = input["type"]
            local conf = input["conf"] or nil
            
            local delivery = DeliveryFactory(type, conf)
            
            delivery:init()
            local data = delivery:read()
            if data then
                if input["format"] then
                    local formatter = FormatterFactory(input["format"])
                    data = formatter:decode(data)
                end

                table.insert(input_data, data)
            end
            delivery:release()
        end

        return input_data
    end

    function operation:run()
        local input_data
        if self.inputs then
            input_data = self:gather_inputs()
        end

        local operation_result = self:func(input_data)
        if self.outputs then
            self:output_result(operation_result)
        end
    end

    function operation:output_result(data)
        for _, output in ipairs(self.outputs) do 
            local type = output["type"]
            local conf = output["conf"] or nil
            local delivery = DeliveryFactory(type, conf)
            
            delivery:init()
            if output["format"] then
                local formatter = FormatterFactory(output["format"])
                data = formatter:encode(data, {indent = true})
            end

            delivery:write(data)
            delivery:release()
        end
    end
    
    return operation
end

return Operation